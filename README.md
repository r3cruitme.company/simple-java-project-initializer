# Simple JAVA project

## Information
In order to be sure that the project will compile for the evaluation, you can test it by yourself with the [ANT](https://ant.apache.org/) module.
You can find a `build.xml` file which explains how to build and exec the project.

The following command will be executed to evaluate the project :
```bash
# To clean the project
ant clean

# To compile the project
ant compile

# To create the jar of the project
ant jar 

# To execute the project from jar
# INPUT corresponds to string inputs for the Main function
java -jar build/jar/project.jar $INPUT
```

The project repository architecture cannot be changed, the Main class needs to be on the `src` folder.